
package entity;

import javax.swing.ImageIcon;

public class Pelicula {
    private int id;
    private String nombre;
    private String horario;
    private ImageIcon imagen;
    private String descripcion;

    public Pelicula(int id, String nombre, String horario, ImageIcon imagen, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.horario = horario;
        this.imagen = imagen;
        this.descripcion = descripcion;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public ImageIcon getImagen() {
        return imagen;
    }

    public void setImagen(ImageIcon imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
