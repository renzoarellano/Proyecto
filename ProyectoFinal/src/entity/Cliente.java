package entity;

public class Cliente {

    private int id;
    private String apellidos;
    private String nombres;
    private String DNI;
    private double precioTotal;
    private int numAMayor;
    private int numAMenor;
    private int numAGen;

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public int getNumAMayor() {
        return numAMayor;
    }

    public void setNumAMayor(int numAMayor) {
        this.numAMayor = numAMayor;
    }

    public int getNumAMenor() {
        return numAMenor;
    }

    public void setNumAMenor(int numAMenor) {
        this.numAMenor = numAMenor;
    }

    public int getNumAGen() {
        return numAGen;
    }

    public void setNumAGen(int numAGen) {
        this.numAGen = numAGen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
